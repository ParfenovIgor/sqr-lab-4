# Lab4 -- Testing coverage 


[![pipeline status](https://gitlab.com/ParfenovIgor/sqr-lab-4/badges/master/pipeline.svg)](https://gitlab.com/ParfenovIgor/sqr-lab-4/-/commits/master)

## Author

Igor Parfenov

Contact: [@Igor_Parfenov](https://t.me/Igor_Parfenov)

## Tests

Created four files with tests:

* `forumDaoUserListTests.java` - branch coverage
* `userDaoChangeTests.java` - mc/dc coverage
* `postDaoSetPostTests.java` - basis path coverage
* `threadDaoTreeSortTests.java` - full statement coverage
