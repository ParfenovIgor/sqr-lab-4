package com.hw.db.controllers;

import com.hw.db.DAO.UserDAO;
import com.hw.db.models.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.lang.Nullable;

import java.util.List;
import java.util.Optional;

class userDaoChangeTests {
    JdbcTemplate mockJdbc;
    UserDAO mockUser;
    static String username;

    @BeforeEach
    void init() {
        username = "Alice";
        mockJdbc = Mockito.mock(JdbcTemplate.class);
        mockUser = new UserDAO(mockJdbc);
    }

    static User makeUser(String email, String fullName, String about) {
        return new User(username, email, fullName, about);
    }

    static List<Arguments> generateArguments() {
        return List.of(
            Arguments.of(makeUser( null,       null,  "about1"), "UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"),
            Arguments.of(makeUser( null,  "Alice A",      null), "UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"),
            Arguments.of(makeUser( null,  "Alice B",  "about2"), "UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;"),
            Arguments.of(makeUser("amail",     null,      null), "UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"),
            Arguments.of(makeUser("bmail",     null,  "about3"), "UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;"),
            Arguments.of(makeUser("cmail", "Alice C",     null), "UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;"),
            Arguments.of(makeUser("dmail", "Alice D", "about4"), "UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;")
        );
    }

    @ParameterizedTest
    @MethodSource("generateArguments")
    void testChangeSuccessful(User user, String expected) {
        UserDAO.Change(user);
        Mockito.verify(mockJdbc).update(Mockito.eq(expected), Optional.ofNullable(Mockito.anyVararg()));
    }

    @Test
    void testChangeNoUpdate() {
        UserDAO.Change(makeUser(null, null, null));
        Mockito.verify(mockJdbc, Mockito.never()).update(Mockito.anyString(), Optional.ofNullable(Mockito.anyVararg()));
    }
}
