package com.hw.db.controllers;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.UserDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.validation.constraints.NotNull;
import java.util.List;

class forumDaoUserListTests {
    JdbcTemplate mockJdbc;
    ForumDAO mockForum;

    @BeforeEach
    void init() {
        mockJdbc = Mockito.mock(JdbcTemplate.class);
        mockForum = new ForumDAO(mockJdbc);
    }

    @NotNull
    static List<Arguments> generateArguments() {
        return List.of(
            Arguments.of("slug1",    1,    null,  true, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname desc LIMIT ?;"),
            Arguments.of("slug2",    2,    null,  null, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname LIMIT ?;"),
            Arguments.of("slug3",    3, "since1", true, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"),
            Arguments.of("slug4",    4, "since2", true, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"),
            Arguments.of("slug5", null,    null,  true, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname desc;"),
            Arguments.of("slug6", null,    null,  null, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;"),
            Arguments.of("slug7", null, "since3", true, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc;"),
            Arguments.of("slug8", null, "since4", null, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;")
        );
    }

    @ParameterizedTest
    @MethodSource("generateArguments")
    void testUserList(String slug, Integer limit, String since, Boolean descending, String expected) {
        ForumDAO.UserList(slug, limit, since, descending);
        Mockito.verify(mockJdbc).query(Mockito.eq(expected), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }
}
