package com.hw.db.controllers;

import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.ThreadDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.validation.constraints.NotNull;
import java.util.List;

public class threadDaoTreeSortTests {
    JdbcTemplate mockJdbc;
    ThreadDAO mockThread;

    @BeforeEach
    void init() {
        mockJdbc = Mockito.mock(JdbcTemplate.class);
        mockThread = new ThreadDAO(mockJdbc);
    }

    static List<Arguments> generateArguments() {
        return List.of(
            Arguments.of(1, null, 4,  true, "SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC ;"),
            Arguments.of(2,    3, 5, false, "SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;")
        );
    }

    @ParameterizedTest
    @MethodSource("generateArguments")
    void testTreeSort(Integer thread, Integer limit, Integer since, Boolean descending, String expected) {
        ThreadDAO.treeSort(thread, limit, since, descending);
        Mockito.verify(mockJdbc).query(Mockito.eq(expected), Mockito.any(PostDAO.PostMapper.class), Mockito.anyVararg());
    }
}
